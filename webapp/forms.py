from django import forms
from .models import Schedule
from django.forms import  ModelForm

class Message_Form(ModelForm):
	class Meta:
		model = Schedule
		fields = ['kegiatan','location','category','tanggal','waktu']
