# Generated by Django 2.1.1 on 2018-10-04 05:41

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Schedule',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('kegiatan', models.CharField(max_length=50)),
                ('location', models.CharField(max_length=50)),
                ('category', models.CharField(max_length=50)),
                ('tanggal', models.DateField(default=django.utils.timezone.now)),
                ('waktu', models.TimeField(default=django.utils.timezone.now)),
            ],
        ),
    ]
