from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.

class Schedule(models.Model):
	kegiatan = models.CharField(max_length=50)
	location = models.CharField(max_length=50)
	category = models.CharField(max_length=50)
	tanggal = models.DateField(default=timezone.now)
	waktu = models.TimeField(default=timezone.now)
