"""projectbang URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from django.conf.urls import include, url
from .views import home
from .views import skills
from .views import portfolio
from .views import tips
from .views import form
from .views import myschedule
from .views import delete_all


urlpatterns = [
	url(r'^home/', home, name='home'),
	url(r'^skills/', skills, name='skills'),
	url(r'^portfolio/', portfolio, name='portfolio'),
	url(r'^tips/', tips, name='tips'),
	url(r'^form/', form, name='form'),
    path('activity/', myschedule, name='activity'),
    url(r'^activity/delete', delete_all, name='deleteall')


] 


