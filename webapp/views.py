from django.shortcuts import render, reverse
from django.http import HttpResponse, HttpResponseRedirect

from .models import Schedule
from .forms import Message_Form

# Create your views here.
def home(request):
	return render(request, 'home.html')
	
def skills(request):
	return render(request, 'skills.html')
	
def portfolio(request):
	return render(request, 'portfolio.html')
	
def tips(request):
	return render(request, 'tips.html')

def form(request):
	return render(request, 'form.html')



def myschedule(request):
	events = Schedule.objects.all().values()

	if request.method == "POST":
		form = Message_Form(request.POST)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect(reverse('webapp:activity'))
	else:
		form = Message_Form()

	response = {'abang': form, 'events': events}
	html = 'page5.html'
	return render(request, html, response)

def delete_all(request):
	Schedule.objects.all().delete()
	return HttpResponseRedirect(reverse('webapp:activity'))
